def dichotonomie():
      a=1
      b=100

      while a < b:
          m = (a + b) // 2
          reponse = input(f"Est-ce que votre nombre est plus grand, plus petit ou égal à {m} ? [+/-/=]")

          if reponse == "=":
              print(f"J'ai trouvé en {a + b} questions !")
              break
          elif reponse == "-":
              b = m - 1
          elif reponse == "+":
              a = m + 1
      if a==b:
          print(f"j'ai trouve, le nombre est {a}")