def jour():
    print("jour de la semaine")
    # Création de la liste des jours de la semaine
    semaine = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]

    # 1. Récupération des 5 premiers jours et des jours du week-end
    premiers_jours = semaine[:5]
    weekend = semaine[5:]

    # Affichage des résultats
    print("1. Les 5 premiers jours de la semaine :", premiers_jours)
    print("   Les jours du week-end :", weekend)

    # 2. Autre manière d'obtenir les 5 premiers jours et les jours du week-end
    premiers_jours_alternatif = semaine[:-2]
    weekend_alternatif = semaine[-2:]

    # Affichage des résultats
    print("2. (Alternatif) Les 5 premiers jours de la semaine :", premiers_jours_alternatif)
    print("   (Alternatif) Les jours du week-end :", weekend_alternatif)

    # 3. Deux manières d'accéder au dernier jour de la semaine
    dernier_jour_methode1 = semaine[-1]
    dernier_jour_methode2 = semaine[len(semaine) - 1]

    # Affichage des résultats
    print("3. (Méthode 1) Le dernier jour de la semaine :", dernier_jour_methode1)
    print("   (Méthode 2) Le dernier jour de la semaine :", dernier_jour_methode2)

    # 4. Inversion des jours de la semaine
    jours_inverses = semaine[::-1]

    # Affichage des résultats
    print("4. Jours de la semaine inversés :", jours_inverses)
