def pendule():
    import random

    # Liste de mots secrets
    mots_secrets = ["dada", "mama", "zoky"]

    # Mot secret
    mot_secret = random.choice(mots_secrets)

    # Liste des caractères découverts
    caracteres_decouverts = []

    # Nombre de fautes
    nombre_fautes = 0

    # Initialise le pendu
    pendu = """
    _________
    |   O
    |  /|\.
    |   |
    |  / \ 
    """

    # Boucle principale
    while nombre_fautes < 5 and mot_secret != "".join(caracteres_decouverts):

        # Affiche le pendu
        print(pendu)

        # Affiche les caractères découverts
        print("".join(caracteres_decouverts))

        # Demande à l'utilisateur de deviner une lettre ou un mot
        saisie_utilisateur = input("Devinez une lettre ou un mot : ")

        # Vérifie si la saisie de l'utilisateur est une lettre
        if len(saisie_utilisateur) == 1:

            # Vérifie si la lettre est correcte
            if saisie_utilisateur in mot_secret:

                # Ajoute la lettre à la liste des caractères découverts
                caracteres_decouverts.append(saisie_utilisateur)

                # Met à jour le pendu
                for i in range(len(mot_secret)):
                    if mot_secret[i] == saisie_utilisateur:
                        pendu = pendu[:i] + saisie_utilisateur + pendu[i + 1:]

            else:

                # Incrémente le nombre de fautes
                nombre_fautes += 1

                # Met à jour le pendu
                pendu = pendu + " | " + "X" + " | " + "X" + " | " + "X"

        else:

            # Vérifie si la saisie de l'utilisateur est le mot secret
            if saisie_utilisateur == mot_secret:

                # Le joueur a gagné
                print("Bravo, vous avez trouvé le mot !")
                break

            else:

                # Incrémente le nombre de fautes
                nombre_fautes += 1

                # Met à jour le pendu
                pendu = pendu + " | " + "X" + " | " + "X" + " | " + "X"

    # Affiche le résultat final
    if nombre_fautes < 5:
        print("Bravo, vous avez trouvé le mot !")
    else:
        print("Désolé, vous avez perdu. Le mot était " + mot_secret)
