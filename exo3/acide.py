def acide():
    print("frequence des acide amine")
    # Séquence d'acides aminés
    sequence_amino_acids = ["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]

    # Fonction pour calculer la fréquence
    def calculer_frequence(sequence, acide_aminé):
        nombre_total = len(sequence)
        nombre_acide_aminé = sequence.count(acide_aminé)
        frequence = nombre_acide_aminé / nombre_total
        return frequence

    # Acides aminés à calculer
    acides_amines_a_calculer = ["A", "R", "W", "G"]

    # Calculer et afficher la fréquence pour chaque acide aminé
    for acide_aminé in acides_amines_a_calculer:
        frequence = calculer_frequence(sequence_amino_acids, acide_aminé)
        print(f"Fréquence de {acide_aminé}: {frequence}")

