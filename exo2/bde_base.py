def bde_base():

    print("en utilisant le boucle for :")

    animaux=["vache" , "souris" , "levure" , "bacterie"]
    for i in animaux:
        print(i)
    
    print("en utilisant le boucle tant que ")
    animal=["vache" , "souris" ,"levure" ,"bacterie"]
    i=0
    #revoye la sequence 
    while i < len(animal):
        print(animal[i])
        i += 1   
        
    print("en utilisant encore un autre methode de for")

    tableau = ["vache", "souris", "levure", "bacterie"]

    # Boucle for
    for i in range(len(tableau)):
        print(tableau[i])
        