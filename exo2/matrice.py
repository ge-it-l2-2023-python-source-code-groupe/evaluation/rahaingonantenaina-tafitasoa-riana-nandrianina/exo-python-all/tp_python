def matrice():

    # Initialisation des variables
    ligne = 0
    hauteur = 0

    # Saisie du nombre de lignes et de colonnes
    ligne = int(input("Entrez le nombre de lignes : "))
    hauteur = int(input("Entrez le nombre de colonnes : "))

    # Initialisation de la matrice
    matrice = [[0 for i in range(hauteur)] for j in range(ligne)]

    # Saisie des valeurs de la matrice
    for i in range(ligne):
        for j in range(hauteur):
            matrice[i][j] = int(input("Entrez la valeur à la position (i, j) : "))

    # Affichage de la matrice
    for i in range(ligne):
        for j in range(hauteur):
            print(matrice[i][j], end=" ")
        print()

    # Saisie de la position de l'élément
    print("Entrez la ligne et la colonne de l'élément à afficher : ")
    ligne = int(input("Ligne : "))
    hauteur = int(input("Colonne : "))

    # Affichage du nombre à la position saisie
    print("Le nombre à la position (", ligne, ",", hauteur, ") est :", (matrice[ligne][hauteur]))
