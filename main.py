from exo1 import affichage,formater,formater2,friedman,polya,polygc,prediction,resultat
from exo2 import bde_base,bde_jour,gauche,impaire,inverser,moyenne,nb10,produit,pyramide,triangle,fibonacy,matrice,puce,diagonal
from exo3 import acide,de,mintableau,paire,pendule,random1,semaine,Syracus,dichotonomie,premier1,premier2,adn,jour,nbpaire,saison,table,liste,range2,etudiant


choice="1"
while (choice!="0"):
    print("\n\n\n***exo1***\n2.11.1--friedman\n2.11.2--prediction\n2.11.3--resultat \n3.6.1-affichage\n3.6.2--polya\n3.6.3--polygc\n3.6.4--formater\n3.6.5--formater2")
    print("\n\n***exo2***\n5.4.1--boucle base\n5.4.2--boucle jour\n5.4.3--nb 1 à 10\n5.4.4--nombre impaire\n5.4.5--moyenne\n5.4.6--produit consecutif\n5.4.7--triangle\n5.4.8--triangle_inverser\n5.4.9--triangle gauche\n5.4.10--pyramide\n5.4.11--Parcours  matrice\n5.4.12--matrice diagonale\n5.4.13--saut de puce\n5.10.14--suite de fibonacci")
    print("\n\n***exo3***\n4.10.1--jour de la semaine\n4.10.2--saison\n4.10.3--table de 9\n4.10.4--nombre impaire\n4.10.5--liste et indice\n4.10.6--liste et range\n\n6.7.1--emploie du temps\n6.7.2--ADN\n6.7.3--minim de tableau\n6.7.4--acide\n6.7.5--mention de note etudiant\n6.7.6--paire\n6.7.7--syracuse\n6.7.9.1--premier1\n6.7.9.2--premier2\n6.7.10--dichotonomie\n\n***exo tp***\ntp1--nombre aleatoire\ntp2--dé\ntp3--pendule")
    

    
    print("la reponse sera afficher en haut ")
    choice = input("saisissez votre exo? ")
    choice=str(choice)
    print("\n\n\n")
    
    if(choice=="2.11.1"):
        print("-----2.11.1-----")
        friedman.friedmnan()
        
    elif(choice=="2.11.2"):
        print("----2.11.2------")
        prediction.prediction()
        
    elif(choice=="2.11.3"):
        print("----2.11.3------")
        resultat.resultat()
        
    elif(choice=="3.6.1"):
        print("---3.6.1---")
        affichage.affichage()
        
    elif(choice=="3.6.2"):
        print("---3.6.2---")
        polya.polya()
        
    elif(choice=="3.6.3"):
        print("---3.6.3---")
        polygc.polygc()
        
    elif(choice=="3.6.4"):
        print("---3.6.4---")
        formater.formater()
        
    elif(choice=="3.6.5"):
        print("---3.6.5---")
        formater2.formater2()
        
    elif(choice=="4.10.1"):
        print("---4.10.1----")
        jour.jour()
        
    elif(choice=="4.10.2"):
       print("---4.10.2---")
       saison.saison()
       
    elif(choice=="4.10.3"):
        print("---4.10.3---")
        table.table()
        
    elif(choice=="4.10.4"):
       print("---4.10.4---")
       nbpaire.nbpaire()
       
    elif(choice=="4.10.5"):
        print("---4.10.5---")
        liste.liste()
        
    elif(choice=="4.10.6"):
        print("---4.10.6---")
        range2.range2()
        
    elif(choice=="5.4.1"):
        print("---5.4.1---")
        bde_base.bde_base()
        
    elif(choice=="5.4.2"):
        print("---5.4.2---")
        bde_jour.bde_jour()
        
    elif(choice=="5.4.3"):
        print("---5.4.3---")
        nb10.nb10()
        
    elif(choice=="5.4.4"):
        print("---5.4.4---")
        impaire.impaire()
        
    elif(choice=="5.4.5"):
        print("---5.4.5---")
        moyenne.moyennes()
        
    elif(choice=="5.4.6"):
        print("---5.4.6---")
        produit.produit()
        
    elif(choice=="5.4.7"):
        print("---5.4.7---")
        triangle.triangle()
        
    elif(choice=="5.4.8"):
        print("---5.4.8---")
        inverser.inverser()
        
    elif(choice=="5.4.8"):
        print("---5.4.9---")
        gauche.gauche()
        
    elif(choice=="5.4.10"):
        print("---5.4.10---")
        pyramide.pyramide()
        
    elif(choice=="5.4.11"):
        print("---5.4.11---")
        matrice.matrice()
        
    elif(choice=="5.4.12"):
        print("---5.4.12---")
        diagonal.diagonal()
        
    elif(choice=="5.4.13"):
        print("---5.4.13---")
        puce.puce()
        
    elif(choice=="5.4.14"):
        print("---5.4.14---")
        fibonacy.fibonacy()
        
    elif(choice=="6.7.1"):
        print("---6.7.1---")
        semaine.semaine()
        
    elif(choice=="6.7.2"):
        print("---6.7.2---")
        adn.adn()
        
    elif(choice=="6.7.3"):
        print("---6.7.3---")
        mintableau.mintableau()
        
    elif(choice=="6.7.4"):
        print("---6.7.4---")
        acide.acide()
        
    elif(choice=="6.7.5"):
        print("---6.7.5---")
        etudiant.etudiant()
        
    elif(choice=="6.7.6"):
        print("---6.7.6---")
        paire.paire()
        
    elif(choice=="6.7.7"):
        print("---6.7.7---")
        Syracus.syracus()
        
    elif(choice=="6.7.8"):
        print("---6.7.8---")
        acide.acide()
        
    elif(choice=="6.7.9.1"):
        print("---6.7.9.1---")
        premier1.premier1()
        
    elif(choice=="6.7.9.2"):
        print("---6.7.9.2---")
        premier2.premier2()
        
    elif(choice=="6.7.10"):
        print("---6.7.10---")
        dichotonomie.dichotonomie()
        
    elif(choice=="tp1"):
        print("---nombre aleatoire---")
        random1=random1()  
          
    elif(choice=="tp2"):
        print("---dé---")
        de.de()
        
    elif(choice=="tp3"):
        print("---pendule---")
        pendule=pendule()   
         
    else:
        print("vous pouvez continué ou vous arreter\nChoisissez un mauvais numero pour arreter le programme!")
        break